# 1) So the main idea is to parse data (text, mark, submarks that leeds to decision) from IELTS essay website
# 2) Use their pretrained bot to classify my text without
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, \
                                       ElementNotInteractableException, \
                                       StaleElementReferenceException, \
                                       ElementClickInterceptedException, \
                                       WebDriverException, \
                                       TimeoutException
from selenium.webdriver import ChromeOptions
from selenium.webdriver.common.by import By
from tqdm import tqdm
from time import sleep
from random import uniform
import pandas as pd
from multiprocessing import Pool, Lock, Manager
import warnings
import logging
import os
import pickle
import secrets

# Hide selenium warnings
logging.getLogger("websockets").setLevel(logging.ERROR)
logging.getLogger("selenium").setLevel(logging.WARNING)
warnings.filterwarnings("ignore")

# define maximum parallel instances number
NUM_INSTANCES = 12


# get common options for chrome
options = ChromeOptions()
options.add_argument('--blink-settings=imagesEnabled=false')
options.add_argument('--headless')
options.add_argument('--disable-extensions')
options.add_argument("user-agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_argument("--log-level=3")  # Set Chrome log level to suppress console messages

# create container to keep links on essay themes
essay_links = []

# keep essay_links
essay_texts = []

# keep parsed data actually used for dataset
data = []


def divide_list(numbers: list, n: int) -> list:
    """
    Divide list 'numbers' into n almost equal parts
    """
    quotient, remainder = divmod(len(numbers), n)
    divided_lists = [numbers[i * quotient + min(i, remainder):(i + 1) * quotient + min(i + 1, remainder)] for i in range(n)]
    return divided_lists


# Wrapper to load existing data or apply function without return
def elders_load_essay_links(serialized_file_name: str, container: list):
    """
    Tries to get data from file and dump data from it into container
    If file is not exist - execute function
    """
    def decorator(func):
        def wrapper(*args, **kwargs):
            nonlocal serialized_file_name
            if os.path.exists(serialized_file_name):
                # File exists, deserialize its content
                with open(serialized_file_name, "rb") as file:
                    container = pickle.load(file)
            else:
                func(*args, **kwargs)
                with open(serialized_file_name, "wb") as file:
                    pickle.dump(container, file)
        return wrapper
    return decorator


@elders_load_essay_links('essay_links.pickle', essay_links)
def get_essay_links() -> list:
    """
    Get links to essays with equal theme
    """
    pbar = tqdm(total=200)
    # setup selenium driver
    driver = webdriver.Chrome(options=options)  # or choose the appropriate webdriver for your browser
    driver.maximize_window()  # Maximizes the browser window
    driver.execute_script("console.log = function() {};")  # Override console.log to suppress messages
    # get links to essay for pages
    driver.get('https://engnovate.com/ugc-ielts-writing-task-2-topics/')
    while True:
        try:
            # get all essay group links
            essay_links.extend([x.get_attribute("href") for x
                               in driver.find_elements(
                                    By.CSS_SELECTOR,
                                    "a.show-samples-button"
                                )])
            # Find the next_button element using CSS selector
            next_button = driver.find_element(By.CSS_SELECTOR,
                                              "a.next.page-numbers")
            # sleep random amount of time
            sleep(uniform(0.1, 1))
            # Click on the next_button element
            next_button.click()
        except (NoSuchElementException, ElementClickInterceptedException,
                TimeoutException, WebDriverException):
            # If the element is not found, break the loop
            break
        pbar.update(1)
        pbar.refresh()
    pbar.close()


def get_essay_texts(list_of_links: list) -> list:
    """
    Get links to each text
    """
    global options
    # Launch webdriver for parsing
    essay_texts_sub = []
    driver = webdriver.Chrome(
        options=options
    )
    # maximize browser window to keep equal names of elements in adaptive page
    driver.maximize_window()
    # Override console.log to suppress messages
    driver.execute_script("console.log = function() {};")
    prev_link = ""
    for essay_link in tqdm(list_of_links, leave=False):
        driver.get(essay_link)
        while True:
            try:
                # Find all elements with the CSS selector "div.read-more-button"
                elements = driver.find_elements(By.CSS_SELECTOR,
                                                "div.read-more-button")

                # Iterate over each element and extract the link
                for element in elements:
                    # Find the anchor element within each div element
                    anchor_element = element.find_element(By.TAG_NAME, "a")

                    # Get the href attribute value and append it to the essay_texts
                    essay_texts_sub.append(anchor_element.get_attribute("href"))
                    # Release the lock to allow other processes to access the list

                # Find the next_button element using CSS selector
                next_button = driver.find_element(By.CSS_SELECTOR,
                                                  "a.next.page-numbers")

                # sleep random amount of time
                sleep(uniform(0.1, 1))

                # Click on the next_button element
                next_button.click()

                if prev_link == essay_link:
                    break
                prev_link = essay_link
            except (NoSuchElementException, TimeoutException,
                    ElementClickInterceptedException, WebDriverException):
                # If the element is not found, break the loop
                # also break if some broke element appears
                break
    driver.close()
    driver.quit()
    return essay_texts_sub


def get_essays_data(essay_texts_links: list) -> list:
    """
    Get data from each essay's link
    """
    global options
    data_sub = []
    # Create an essay:
    driver = webdriver.Chrome(
        options=options
    )
    driver.maximize_window()  # Maximizes the browser window
    driver.execute_script("console.log = function() {};")  # Override console.log to suppress messages
    file_name = f"out_{secrets.token_hex(16)}.pickle"
    for i, essay_text in tqdm(enumerate(essay_texts_links), leave=False):
        driver.get(essay_text)
        try:
            row_dict = {
                'title': driver.find_element(
                        By.CSS_SELECTOR, "header.entry-header"
                    ).find_element(By.TAG_NAME, "h1").text,
                'text': driver.find_element(By.CSS_SELECTOR,
                                            "div.entry-content").text,
                'overall_score': driver.find_element(
                        By.CSS_SELECTOR,
                        "strong#overall-band-score"
                    ).text,
                'vocabulary_level': driver.find_element(
                        By.CSS_SELECTOR, "div.vocabulary-level"
                    ).find_element(By.TAG_NAME, "span").text,
                'grammar_mistakes': driver.find_element(
                        By.CSS_SELECTOR, "div.grammar-mistakes"
                    ).find_element(By.TAG_NAME, "span").text,
                'task_response': driver.find_element(
                        By.CSS_SELECTOR, "strong#task-response-score"
                    ).text,
                'task_response_description': driver.find_element(
                        By.CSS_SELECTOR, "div#task-response-description"
                    ).text,
                'coherence_and_cohesion': driver.find_element(
                        By.CSS_SELECTOR, "strong#coherence-cohesion-score"
                    ).text,
                'coherence_and_cohesion_description': driver.find_element(
                        By.CSS_SELECTOR, "div#coherence-cohesion-description"
                    ).text,
                'lexical_resource': driver.find_element(
                        By.CSS_SELECTOR, "strong#lexical-resource-score"
                    ).text,
                'lexical_resource_description': driver.find_element(
                        By.CSS_SELECTOR, "div#lexical-resource-description"
                    ).text,
                'grammatical_range_and_accuracy': driver.find_element(
                        By.CSS_SELECTOR,
                        "strong#grammatical-range-accuracy-score"
                    ).text,
                'grammatical_range_and_accuracy_text': driver.find_element(
                        By.CSS_SELECTOR,
                        "div#grammatical-range-accuracy-description"
                    ).text,
            }
            data_sub.append(row_dict)
            # sleep random amount of time
            sleep(uniform(0.1, 1))
            if i % 50 == 49:
                with open(file_name, "wb") as file:
                    pickle.dump(data_sub, file)
            if i == len(essay_texts_links)-1:
                with open(file_name, "wb") as file:
                    pickle.dump(data_sub, file)
                driver.close()
                driver.quit()
                return data_sub
        except (NoSuchElementException, TimeoutException,
                ElementClickInterceptedException, WebDriverException):
            # If the element is not found, break the loop
            # also break if some broke element appears
            with open(file_name, "wb") as file:
                pickle.dump(data_sub, file)
            driver.close()
            driver.quit()
            return data_sub

    driver.close()
    driver.quit()
    with open(file_name, "wb") as file:
        pickle.dump(data_sub, file)
    return data_sub


if __name__ == "__main__":
    # Create a multiprocessing manager
    manager = Manager()

    # keep essay_links
    essay_texts = manager.list()

    # keep parsed data actually used for dataset
    data = manager.list()

    # Load first essay links list:
    get_essay_links()

    # After getting all main links, use pool for selenium instances
    half_file_passed_name = "essay_texts.pickle"
    if os.path.exists(half_file_passed_name):
        # File exists, deserialize its content
        with open(half_file_passed_name, "rb") as file:
            essay_texts = pickle.load(file)
    else:
        p1 = Pool(processes=NUM_INSTANCES)
        result1 = p1.map(get_essay_texts, divide_list(essay_links, NUM_INSTANCES))
        # Wait for the processes to complete and get the result
        # Close the pool to prevent further task submission
        p1.close()
        # Wait for all tasks to complete
        p1.join()

        # join all results
        for result in result1:
            essay_texts.extend(result)
        essay_texts = list(essay_texts)

        with open(half_file_passed_name, "wb") as file:
            pickle.dump(essay_texts, file)

    # Iterate over the files in the directory
    file_list_of_pickled_data = []
    for filename in os.listdir('./'):
        if filename.startswith("out") and filename.endswith(".pickle"):
            file_list_of_pickled_data.append(filename)

    if file_list_of_pickled_data is not None:
        data = []
        # Deserialize and extend the data into one list
        for file in file_list_of_pickled_data:
            with open(file, 'rb') as f:
                data_part = pickle.load(f)
                data.extend(data_part)
    else:
        # After getting all main links, use pool for selenium instances
        p2 = Pool(processes=NUM_INSTANCES)
        result2 = p2.map(get_essays_data, divide_list(essay_texts, NUM_INSTANCES))
        # Close the pool to prevent further task submission
        p2.close()
        # Wait for all tasks to complete
        p2.join()
        # join all results
        for result in result2:
            data.extend(result)
        data = list(data)

    # Put collacted data into DataFrame
    df = pd.DataFrame(data)
    # save this table as csv format
    df.to_csv('collected_ielts_data.csv')
