# TO USE THIS CODE DOWNLOAD WORD2VEC WEIGHTS FROM https://www.kaggle.com/datasets/sugataghosh/google-word2vec
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from wordcloud import WordCloud

# get text readability scores
import textstat
import string
from gensim.models import Word2Vec, KeyedVectors
import spacy
from scipy import spatial
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.decomposition import PCA

from collections import Counter


# Load the English tokenizer, tagger, parser, NER, and word vectors
nlp = spacy.load('en_core_web_sm')
nltk.download('wordnet')


def preprocess_text(text: str,
                    lemmatize: bool = True,
                    remove_stopwords: bool = True) -> list:
    """
    Make all that possible to filter text text from messy data and stop-words
    """
    # Tokenize the text into words
    words = word_tokenize(text)
    processed_words = []
    stop_words = set(stopwords.words('english'))
    for word in words:
        # Remove punctuation and convert to lowercase
        processed_word = "".join(char.lower() for char in word if char.isalnum())
        # Lemmatize if enabled
        if lemmatize:
            processed_word = nltk.WordNetLemmatizer().lemmatize(processed_word)
        # Remove stopwords if enabled
        if remove_stopwords and processed_word.lower() in stop_words:
            continue
        # Skip empty strings and space symbols
        if len(processed_word) > 0 and processed_word != ' ':
            processed_words.append(processed_word)
    return processed_words


def get_word_vectors(word_list: list, word2vec_model: dict) -> np.array:
    """
    Divide text by words, apply word2vec for them and return list of it
    """
    vectors = []
    for word in word_list:
        if word in word2vec_model:
            vectors.append(word2vec_model[word])
        else:
            # Handle out-of-vocabulary words here
            vectors.append(np.zeros(word2vec_model.vector_size))
    return np.array(vectors)


def calculate_similarity(text1: str, text2: str) -> tuple:
    """
    Calsulate cosine similarity on centroid vector of each texts
    """
    processed_text1, processed_text2 = (preprocess_text(text)
                                        for text in (text1, text2))

    model = KeyedVectors.load_word2vec_format(
        'GoogleNews-vectors-negative300.bin',
        binary=True
    )

    vectors1, vectors2 = (get_word_vectors(text, model)
                          for text in (processed_text1, processed_text2))

    print(f"Vector shapes are: {vectors1.shape}, {vectors2.shape}")
    print(f"Non-null element count: {np.count_nonzero(vectors1 != 0)}, {np.count_nonzero(vectors2 != 0)}")

    # Find sum of vectors in text (get result vector)
    centroid_vector1, centroid_vector2 = (np.sum(vector, axis=0)
                                          for vector in (vectors1, vectors2))
    mean_vector1, mean_vector2 = (np.mean(vector, axis=0)
                                  for vector in (vectors1, vectors2))

    cosine_similarity_centroids = 1 - spatial.distance.cosine(centroid_vector1,
                                                              centroid_vector2)
    cosine_similarity_mean = 1 - spatial.distance.cosine(mean_vector1,
                                                         mean_vector2)
    return cosine_similarity_centroids, cosine_similarity_mean


def plot_text_timeseries(text: str) -> None:
    processed_text = preprocess_text(text)
    model = KeyedVectors.load_word2vec_format(
        'GoogleNews-vectors-negative300.bin',
        binary=True
    )
    vectors = get_word_vectors(processed_text, model)
    # Reshape the time series array to match the input shape required by PCA
    reshaped_series = np.concatenate(vectors).reshape(len(vectors), -1)
    # Perform PCA to extract the principal component scores
    pca = PCA(n_components=1)
    downscaled_series = pca.fit_transform(reshaped_series)
    # Create x-axis values for the line plot
    x_values = np.arange(len(downscaled_series))
    # Plot the downsampled time series
    plt.plot(x_values, downscaled_series)
    # Add labels and title to the plot
    plt.xlabel('Time')
    plt.ylabel('Value')
    plt.title('Time Series')


def plot_word_cloud(text: str) -> None:
    """Plots Word Cloud Plot, that represents word frequency in corpus"""
    # Generate word cloud
    wordcloud = WordCloud(width=800,
                          height=400,
                          background_color='white').generate(text)
    # Create the plot
    plt.figure(figsize=(10, 5))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')


def bar_chart_plot(test: str, num_words: int = 15, plot_type: str = 'bar') -> None:
    """PLot graph with frequenses of each word"""
    # Tokenize the text into words
    words = text.split()
    # Calculate word frequencies
    word_freq = Counter(words)
    top_words = dict(word_freq.most_common(num_words))
    # Prepare data for plotting
    x = list(top_words.keys())
    y = list(top_words.values())
    # Create the plot
    plt.figure(figsize=(10, 6))
    if plot_type == 'bar':
        # Bar plot
        plt.bar(x, y)
        plt.xlabel('Words')
        plt.ylabel('Frequency')
    elif plot_type == 'histogram':
        # Histogram
        plt.hist(words, bins=num_words, edgecolor='black')
        plt.xlabel('Words')
        plt.ylabel('Frequency')
    plt.title('Word Frequencies')
    plt.xticks(rotation=45)
    plt.tight_layout()


if __name__ == '__main__':
    stop_words = set(stopwords.words('english'))
    nlp = spacy.load('en_core_web_sm')

    with open('test_sample.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
        name = lines[0].strip()
        text = " ".join(lines[1:]).strip()

    map_of_criterions = lambda test_data: {
        "Flesch Reading Ease": textstat.flesch_reading_ease(test_data),
        "Flesch Kincaid Grade": textstat.flesch_kincaid_grade(test_data),
        "Gunning Fog": textstat.gunning_fog(test_data),
        "Coleman Liau Index": textstat.coleman_liau_index(test_data),
        "Linsear Write Formula": textstat.linsear_write_formula(test_data),
    }

    list_of_difficult_words = textstat.difficult_words_list(text)

    print(name)

    for key, item in map_of_criterions(text).items():
        print(key, 'is:', item)
    print("Difficult words are:", ', '.join(list_of_difficult_words))

    similarity = calculate_similarity(name, text)

    print(f"Similarity score: {similarity}")

    plot_text_timeseries(text)
    plot_word_cloud(text)
    bar_chart_plot(text)
    plt.show()
