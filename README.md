# Automatic Essay Evaluation

This project aims to develop an automatic essay evaluation system using NLP algorithms. The goal is to create a system that can analyze and assess essays based on various criteria.

## Getting Started

To get started with GitLab and this project, follow the steps below:

1. Clone the repository using the following command:
   ```shell
   git clone git@gitlab.com:ArsenyCheplyukov/automatic-essay-evaluation.git
   ```

2. Create and activate a virtual environment (venv) for the project:
   ``` shell
   python -m venv venv         # Create a virtual environment
   venv/bin/activate   # Activate the virtual environment (for Unix/Linux)
   venv\Scripts\activate      # Activate the virtual environment (for Windows)
   ```

3. Install the project dependencies from the `requirements.txt` file:
   ``` shell
   pip install -r requirements.txt
   ```

4. Add the necessary datasets to the project. Download the following datasets from their respective sources:

   - [ASAP-AES Dataset](https://www.kaggle.com/competitions/asap-aes/data)
   - [IELTS Writing Scored Essays Dataset](https://www.kaggle.com/datasets/mazlumi/ielts-writing-scored-essays-dataset/data)
   - [DAIGT V2 Train Dataset](https://www.kaggle.com/datasets/thedrcat/daigt-v2-train-dataset/data) (Generated LLM dataset)
   - [Feedbackprize Full Essays Dataset](https://www.kaggle.com/datasets/yujikomi/feedbackprize-full-essays)
   - [EssayForum-Dataset](https://huggingface.co/datasets/nid989/EssayFroum-Dataset/tree/main)
   - [Essay Index Dataset](https://huggingface.co/datasets/ChristophSchuhmann/essay-index/tree/main)
   - [EssayForum Raw Writing 10K Dataset](https://huggingface.co/datasets/dim/essayforum_raw_writing_10k/tree/main/data)

   Place the downloaded datasets in the appropriate directories within the project (names may differ).

5. Once the project is set up and the datasets are in place, you can proceed with the development and visualization of the datasets.

## Project Structure

The project is organized as follows:

- `README.md`: This file provides an overview of the project and instructions on how to get started.
- `LICENSE`: The project is licensed under the MIT License.
- Other project files and directories can be found within the cloned repository.

## Authors

- Arseny Cheplyukov

In addition to the project author, it is important to acknowledge the authors and sources of the downloaded datasets mentioned above. Please refer to the respective sources for information on licensing and authors of these datasets.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.

## Project Status

Currently, the project is in the development phase. Additional work is required to implement and refine the NLP algorithms for automatic essay evaluation. Contributions and collaborations are welcome.

If you have any questions or need further assistance, please feel free to reach out.